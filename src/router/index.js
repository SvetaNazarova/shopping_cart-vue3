import { createRouter, createWebHistory } from "vue-router"
import HomeVue from '../views/HomeVue.vue'
import CartVue from '../views/CartVue.vue'

const routes = [
  { path: '/', name: 'HomeVue', component: HomeVue },
  { path: '/cart', name: 'CartVue', component: CartVue },
]

const router = createRouter({
  
  history: createWebHistory(process.env.BASE_URL),
  routes, 
})

export default router