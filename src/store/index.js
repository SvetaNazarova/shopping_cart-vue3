import { createStore } from "vuex"

function updateLocalStorage(cartVue) {
    localStorage.setItem('cartVue', JSON.stringify(cartVue))
}

export default createStore({
    state: {
        cartVue: []
    },
    getters: {
        productQuantity: state => product => {
            const item = state.cartVue.find(i => i.id === product.id)

            if (item) return item.quantity
            else return null
        },
        cartItems: state => {
            return state.cartVue
        },
        cartTotal: state => {
            return state.cartVue.reduce((a, b) => a + (b.price * b.quantity), 0)


        }
    },
    mutations: {
        addToCart(state, product) {
            let item = state.cartVue.find(i => i.id === product.id)

            if (item) {
                item.quantity++
            } else {
                state.cartVue.push({ ...product, quantity: 1 })
            }

            updateLocalStorage(state.cartVue)
        },
        removeFromCart(state, product) {
            let item = state.cartVue.find(i => i.id === product.id)

            if (item) {
                if (item.quantity > 1) {
                    item.quantity--
                } else {
                    state.cartVue = state.cartVue.filter(i => i.id !== product.id)
                }
            }
            updateLocalStorage(state.cartVue)
        },
        updateCartFromLocalStorage(state) {
            const cartVue = localStorage.getItem('cartVue')
            if (cartVue) {
                state.cartVue = JSON.parse(cartVue)
            }
        }
    },
    actions: {

    },
    modules: {

    }
})